package tk.huunghia.dagger_hilt.ui.main.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import tk.huunghia.dagger_hilt.R
import tk.huunghia.dagger_hilt.data.model.Prize
import tk.huunghia.dagger_hilt.databinding.ActivityMainBinding
import tk.huunghia.dagger_hilt.di.ApiKey
import tk.huunghia.dagger_hilt.di.LibraryKey
import tk.huunghia.dagger_hilt.ui.main.adapter.PrizeAdapter
import tk.huunghia.dagger_hilt.ui.main.viewmodel.MainViewModel
import tk.huunghia.dagger_hilt.utils.Status
import tk.huunghia.dagger_hilt.utils.recyclerview.RecyclerViewManager
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val mainViewModel: MainViewModel by viewModels()
    private lateinit var prizeAdapter: PrizeAdapter

    @ApiKey
    @Inject lateinit var apiKey: String

    @LibraryKey
    @Inject lateinit var libraryKey: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        binding.executePendingBindings()
        binding.viewModel = mainViewModel
        setupUI()
        setupObserver()

        Log.i("MainActivity", "onCreate: ${apiKey} - ${libraryKey}")
    }

    private fun setupObserver() {
        mainViewModel.prizesObserver.observe(this){
            when (it.status){
                Status.SUCCESS -> {
                    it.data?.let { it1 -> renderListPrizes(it1) }
                    binding.loading = false
                }
                Status.LOADING -> {
                    binding.loading = true
                }
                Status.ERROR -> {
                    binding.loading = false
                }
            }
        }
    }

    private fun renderListPrizes(prizes: List<Prize>) {
        prizeAdapter.addData(prizes)
        prizeAdapter.notifyDataSetChanged()
    }

    private fun setupUI() {
        binding.rvPrizes.layoutManager = LinearLayoutManager(this)
        binding.rvPrizes.addItemDecoration(DividerItemDecoration(this,
            (binding.rvPrizes.layoutManager as LinearLayoutManager).orientation)
        )
        prizeAdapter = PrizeAdapter()
        binding.rvPrizes.adapter = prizeAdapter

        RecyclerViewManager(binding.rvPrizes)
    }
}