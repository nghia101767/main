package tk.huunghia.dagger_hilt.ui.main.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope
import tk.huunghia.dagger_hilt.data.defined.ParamKey
import tk.huunghia.dagger_hilt.data.model.Prize
import tk.huunghia.dagger_hilt.data.model.User
import tk.huunghia.dagger_hilt.data.repository.MainRepository
import tk.huunghia.dagger_hilt.utils.NetworkHelper
import tk.huunghia.dagger_hilt.utils.Resource
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val mainRepository: MainRepository,
    private val networkHelper: NetworkHelper
) :
    ViewModel() {
    //
    private val _userObserver = MutableLiveData<Resource<List<User>>>().apply {}
    val usersObserver: LiveData<Resource<List<User>>>
        get() = _userObserver
    //
    private val _prizesObserver = MutableLiveData<Resource<List<Prize>>>().apply {}
    val prizesObserver: LiveData<Resource<List<Prize>>>
        get() = _prizesObserver

    init {
        fetchPrizes()
    }
    private fun fetchPrizes() {
        viewModelScope.launch {
            supervisorScope {
                if (networkHelper.isNetworkConnected()) {
                    _prizesObserver.postValue(Resource.loading(null))
                    try {
                        val params: HashMap<String, String> = hashMapOf()
                        params[ParamKey.PARAMS_PER_PAGE] = "10"
                        params[ParamKey.PARAMS_PAGE] = "1"
                        mainRepository.getPrizes(params).let {
                            if (it.isSuccessful) {
                                _prizesObserver.postValue(Resource.success(it.body()))
                            } else _prizesObserver.postValue(
                                Resource.error(
                                    it.errorBody().toString(),
                                    null
                                )
                            )
                        }
                    }catch (e: Exception){
                        Log.e("MainViewModel", "fetchPrizes: ", e)
                        _prizesObserver.postValue(Resource.error("No network connection", null))
                    }
                } else {
                    _prizesObserver.postValue(Resource.error("No network connection", null))
                }
            }
        }
    }
}