package tk.huunghia.dagger_hilt.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import tk.huunghia.dagger_hilt.data.model.Prize
import tk.huunghia.dagger_hilt.databinding.PrizeItemBinding

class PrizeAdapter: RecyclerView.Adapter<PrizeAdapter.ViewHolder>(){
    private val prizes: ArrayList<Prize> = arrayListOf()
    class ViewHolder(val binding: PrizeItemBinding): RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = PrizeItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.prize = prizes[position]
    }

    override fun getItemCount() = prizes.count()

    fun addData(prizes: List<Prize>) {
        this.prizes.addAll(prizes)
    }

}