package tk.huunghia.dagger_hilt.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import tk.huunghia.dagger_hilt.data.model.User
import tk.huunghia.dagger_hilt.databinding.UserItemBinding

class MainAdapter: RecyclerView.Adapter<MainAdapter.ViewHolder>(){
    private val users: ArrayList<User> = arrayListOf()
    class ViewHolder(val binding: UserItemBinding): RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = UserItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.user = users[position]
    }

    override fun getItemCount() = users.count()

    fun addData(users: List<User>) {
        this.users.addAll(users)
    }

}