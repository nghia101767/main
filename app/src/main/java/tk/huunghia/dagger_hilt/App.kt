package tk.huunghia.dagger_hilt

import android.app.Application
import com.bumptech.glide.Glide
import com.bumptech.glide.MemoryCategory
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App: Application() {
    override fun onCreate() {
        super.onCreate()
        Glide.get(applicationContext)
            .setMemoryCategory(MemoryCategory.HIGH)
    }
}