package tk.huunghia.dagger_hilt.data.defined

class ParamKey {
    companion object{
        const val PARAMS_PER_PAGE = "per_page"
        const val PARAMS_PAGE = "page"
    }
}