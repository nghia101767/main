package tk.huunghia.dagger_hilt.data.repository

import tk.huunghia.dagger_hilt.data.api.ApiHelper
import javax.inject.Inject

class MainRepository @Inject constructor(private val apiHelper: ApiHelper) {
    suspend fun getUsers() = apiHelper.getUsers()
    suspend fun getPrizes(params: HashMap<String, String>) = apiHelper.getPrizes(params)
}