package tk.huunghia.dagger_hilt.data.api

import retrofit2.Response
import tk.huunghia.dagger_hilt.data.model.Prize
import tk.huunghia.dagger_hilt.data.model.User

interface ApiHelper {
    suspend fun getUsers(): Response<List<User>>
    suspend fun getPrizes(params: HashMap<String, String>): Response<List<Prize>>
}