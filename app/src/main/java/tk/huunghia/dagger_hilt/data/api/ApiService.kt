package tk.huunghia.dagger_hilt.data.api

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.QueryMap
import tk.huunghia.dagger_hilt.data.model.Prize
import tk.huunghia.dagger_hilt.data.model.User

interface ApiService {
    @GET("users")
    suspend fun getUsers(): Response<List<User>>
    @GET("games")
    suspend fun getPrizes(@QueryMap map: HashMap<String, String>): Response<List<Prize>>
}