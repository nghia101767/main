package tk.huunghia.dagger_hilt.data.api

import retrofit2.Response
import tk.huunghia.dagger_hilt.data.model.Prize
import tk.huunghia.dagger_hilt.data.model.User
import javax.inject.Inject

class ApiHelperImpl @Inject constructor(private val apiService: ApiService): ApiHelper {
    override suspend fun getUsers(): Response<List<User>>  = apiService.getUsers()
    override suspend fun getPrizes(params: HashMap<String, String>): Response<List<Prize>>  = apiService.getPrizes(params)
}