package tk.huunghia.dagger_hilt.data.model

import com.squareup.moshi.Json

data class Prize(
    @Json(name = "ID")
    val id: String = "0",
    @Json(name = "name")
    val name: String = "",
    @Json(name = "prize_status")
    val prize_status: String = "",
    @Json(name = "post_date")
    val post_date: String = "",
    @Json(name = "prize_game_id")
    val prize_game_id: String = "",
    @Json(name = "prize_freeplay")
    val prize_freeplay: String = "",
    @Json(name = "point_yp")
    val point_yp: String = "",
    @Json(name = "prize_bp")
    val prize_bp: String = "",
    @Json(name = "prize_red_ticket")
    val prize_red_ticket: String = "",
    @Json(name = "prize_free_play")
    val prize_free_play: String = "",
    @Json(name = "prize_counter")
    val prize_counter: String = "",
    @Json(name = "game_play_status")
    val game_play_status: String = "",
    @Json(name = "game_close")
    val game_close: String = "",
    @Json(name = "position_order")
    val position_order: String = "",
    @Json(name = "post_title")
    val post_title: String = "",
    @Json(name = "img")
    val img: String = "",
    @Json(name = "game_status")
    val game_status: String = "",
    @Json(name = "discount_yp")
    val discount_yp: String = "",
    @Json(name = "discount_bp")
    val discount_bp: String = "",
    @Json(name = "top_page")
    val top_page: String = ""
)
//"ID": "97457",
//"name": "【橋渡しB】Re:ゼロから始める異世界生活　プレシャスフィギュア レム～スポーティサマーver.～",
//"prize_status": "publish",
//"post_date": "2021-06-25 22:41:11",
//"prize_game_id": "1159",
//"prize_freeplay": "0",
//"point_yp": "180",
//"prize_bp": "-1",
//"prize_red_ticket": "1",
//"prize_free_play": "0",
//"prize_counter": "2",
//"game_play_status": "プ レ イ 中",
//"game_close": "",
//"position_order": "1",
//"post_title": "No.１０Ｌ_6",
//"post_name": "%ef%bc%96%ef%bc%94%ef%bc%91",
//"img": "http://yattare.jp/wp-content/uploads/2021/06/www.taito_.co_-150x150.jpg",
//"game_status": "playing",
//"discount_yp": "144",
//"discount_bp": "0",
//"top_page": ""