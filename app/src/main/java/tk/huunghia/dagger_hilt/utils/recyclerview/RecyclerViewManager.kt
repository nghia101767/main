package tk.huunghia.dagger_hilt.utils.recyclerview

import android.view.MotionEvent
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewManager(private val recyclerView: RecyclerView) {
    init {
        setupListener()
    }

    private fun setupListener() {
        scrollListener()
        itemSelectListener()
    }

    private fun scrollListener() {
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
        })
    }

    private fun itemSelectListener() {
        recyclerView.addOnItemTouchListener(object : RecyclerView.OnItemTouchListener{
            override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
                return false //true can not touch
            }

            override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {

            }

            override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {

            }

        })
    }
}