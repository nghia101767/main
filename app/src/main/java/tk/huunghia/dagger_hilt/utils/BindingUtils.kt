package tk.huunghia.dagger_hilt.utils

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import tk.huunghia.dagger_hilt.R

@BindingAdapter("android:hide")
fun hideMe(view: View, hide: Boolean = false) {
    view.visibility = if (hide) {
        View.GONE
    } else {
        View.VISIBLE
    }
}

@BindingAdapter("android:loadImage")
fun loadImage(view: ImageView, url: String) {
    Glide.with(view.context)
        .load(url)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
//        .placeholder(ColorDrawable(Color.GRAY))
        .placeholder(R.drawable.loading)
//        .error(ColorDrawable(Color.RED))
        .error(R.drawable.no_image)
        .into(view)
}