package tk.huunghia.dagger_hilt.utils

enum class Status {
    SUCCESS, ERROR, LOADING
}